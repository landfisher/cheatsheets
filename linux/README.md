# Sysadmin
## Show UDP network ports running and associated services
## This is useful to identy DNS related traffic/services
~~~
sudo netstat -anp | grep -i udp
~~~

## Stop systemd-resolve and disable it
~~~
sudo systemctl stop systemd-resolved
sudo systemctl disable systemd-resolved
~~~

## Show status of dnsmasq
~~~
sudo systemctl status dnsmasq
~~~

## MicroK8/Kubernetes Commands
### List out all configured Namespaces
~~~
microk8s kubectl get all --all-namespaces
~~~
### Enable basic services
~~~
microk8s enable dns dashboard dashboard-ingress rbac storage
~~~
### Enable 'Traefik'
~~~
microk8s enable traefik
~~~
### Setup 'Portainer Agent' for Environment Connection (don't run as sudo or root)
~~~
microk8s kubectl apply -f https://downloads.portainer.io/ce2-16/portainer-agent-k8s-nodeport.yaml
~~~
### Disable MicroK8s DNS/Traefik (if needed to de-conflict w/ Pihole/Traefik containers running on Docker instead)
~~~
microk8s disable dns
microk8s disable traefik
~~~